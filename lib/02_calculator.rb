def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(numbers)
  numbers.size==0 ? 0 : numbers.reduce(:+)
end

def multiply(*numbers)
  numbers_array = *numbers
  numbers_array.reduce(1, :*)
end

def power(num, exponent)
  num**exponent
end

def factorial(num)
  num < 2 ? 1 : (1..num).reduce(1, :*)
end
