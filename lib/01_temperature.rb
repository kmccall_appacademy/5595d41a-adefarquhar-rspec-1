def ftoc(fahrenheit)
  celsius = (fahrenheit - 32)/1.8
  celsius.round
end

def ctof(celsius)
  fahrenheit = 1.8*celsius + 32
end
