def echo(says)
  says
end

def shout(says)
  says.upcase
end

def repeat(says, times=2)
  repeated = "#{says} "*times
  repeated.chop
end

def start_of_word(word, first_letters=1)
  word.slice(0, first_letters)
end

def first_word(says)
  words = says.split
  words[0]
end

def titleize(says)
  words = says.split
  words.each do |word|
    word.capitalize! if word==words.first || word==words.last || word.length>4
  end
  words.join(" ")
end
