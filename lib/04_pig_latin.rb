def begins_vowel(word)
  word+'ay'
end

def begins_consonant(word)
  word[1..-1] + word[0] + 'ay'
end

def begins_2consonants(word)
  word[2..-1] + word[0..1] + 'ay'
end

def begins_3consonants(word)
  word[3..-1] + word[0..2] + 'ay'
end

def pig_latin(word)
  return begins_vowel(word) if (word =~ /\A[aeiou]/) == 0
  return begins_3consonants(word) if (word[0..2] =~ /[aeiou]/) == nil
  return begins_3consonants(word) if (word =~ /\A[^aeiou]/) == 0 && word[1..2] == 'qu'
  return begins_2consonants(word) if (word[0..1] =~ /[aeiou]/) == nil || word[0..1] == 'qu'
  return begins_consonant(word) if (word =~ /\A[^aeiou]/) == 0
end

def translate(words)
  words = words.split
  words.collect! { |word| pig_latin(word) }
  words.join(' ')
end
